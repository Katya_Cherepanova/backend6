<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
	if (!empty($_COOKIE['save'])) {
      setcookie('save', '', 100000);
      setcookie('login', '', 100000);
      setcookie('pass', '', 100000);
    	$messages[] = 'Данные сохранены';

      if (!empty($_COOKIE['pass'])) {
        $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
          и паролем <strong>%s</strong> для изменения данных.',
          strip_tags($_COOKIE['login']),
          strip_tags($_COOKIE['pass']));
      }
  	}
    $errors = array();
    $errors['field-name'] = !empty($_COOKIE['field-name_error']);
    $errors['field-email'] = !empty($_COOKIE['field-email_error']);
    $errors['field-date'] = !empty($_COOKIE['field-date_error']);
    $errors['radio-group-1'] = !empty($_COOKIE['radio-group-1_error']);
    $errors['radio-group-2'] = !empty($_COOKIE['radio-group-2_error']);
    $errors['field-name-4'] = !empty($_COOKIE['field-name-4_error']);
    $errors['field-name-2'] = !empty($_COOKIE['field-name-2_error']);
    $errors['check-1'] = !empty($_COOKIE['check-1_error']);
    $errors['1'] = !empty($_COOKIE['1_error']);
    $errors['2'] = !empty($_COOKIE['2_error']);
    $errors['3'] = !empty($_COOKIE['3_error']);

    if ($errors['field-name']) {
      setcookie('field-name_error', '', 100000);
      $messages[] = '<div class="error">Ввод имени.</div>';
    }
    if ($errors['1']){
      setcookie('1_error', '', 100000);
      $messages[] = '<div class="error">Введите имя с использованием латинских букв</div>';
    } 
    if ($errors['field-email']) {
      setcookie('field-email_error', '', 100000);
      $messages[] = '<div class="error">Ввод e-mail.</div>';
    }
    if ($errors['2']){
      setcookie('2_error', '', 100000);
      $messages[] = '<div class="error">Введите название почты,например:cherepanova@mail.ru </div>';
    } 
    if ($errors['field-date']) {
      setcookie('field-date_error', '', 100000);
      $messages[] = '<div class="error">Ввод даты.</div>';
    }
    if ($errors['3']){
      setcookie('3_error', '', 100000);
      $messages[] = '<div class="error">Введите дату, например: 08.08.2001</div>';
    } 
    if ($errors['radio-group-1']) {
      setcookie('radio-group-1_error', '', 100000);
      $messages[] = '<div class="error">Выбор пола.</div>';
    }
    if ($errors['radio-group-2']) {
      setcookie('radio-group-2_error', '', 100000);
      $messages[] = '<div class="error">Выбор кол-ва конечностей.</div>';
    }
    if ($errors['field-name-4']) {
      setcookie('field-name-4_error', '', 100000);
      $messages[] = '<div class="error">Выбор суперспособности.</div>';
    }
    if ($errors['field-name-2']) {
      setcookie('field-name-2_error', '', 100000);
      $messages[] = '<div class="error">Напишите биографию.</div>';
    }
    if ($errors['check-1']) {
      setcookie('check-1_error', '', 100000);
      $messages[] = '<div class="error">Примите условия.</div>';
    }
  
    $values = array();

    $values['field-name'] = empty($_COOKIE['field-name_value']) ? '' : $_COOKIE['field-name_value'];
    $values['field-email'] = empty($_COOKIE['field-email_value']) ? '' : $_COOKIE['field-email_value'];
    $values['field-date'] = empty($_COOKIE['field-date_value']) ? '' : $_COOKIE['field-date_value'];
    $values['radio-group-1'] = empty($_COOKIE['radio-group-1_value']) ? '' : $_COOKIE['radio-group-1_value'];
    $values['radio-group-2'] = empty($_COOKIE['radio-group-2_value']) ? '' : $_COOKIE['radio-group-2_value'];
    $values['field-name-4'] = empty($_COOKIE['field-name-4_value']) ? '' : $_COOKIE['field-name-4_value'];
    $values['field-name-2'] = empty($_COOKIE['field-name-2_value']) ? '' : $_COOKIE['field-name-2_value'];
    $values['check-1'] = empty($_COOKIE['check-1_value']) ? '' : $_COOKIE['check-1_value'];
  
    $flag = FALSE;
  foreach($errors as $er){
    if(!empty($er)){
      $flag = TRUE;
      break;
    }
    print($er);
  }

  if (!$flag && !empty($_COOKIE[session_name()]) &&
  session_start() && !empty($_SESSION['login'])) { 
        try {
        $user = 'u23970';
        $pass1 = '9866387';
        $db = new PDO('mysql:host=localhost;dbname=u23970', $user, $pass1, array(PDO::ATTR_PERSISTENT => true));
        $log1 = $_SESSION['login'];
        $pass24 = $_SESSION['pass'];
          $data = $db->query("SELECT * FROM form where login = '$log1' AND pass='$pass24'");  

          foreach ($data as $row) {
            $values['field-name'] = $row['imya'];
            $values['field-email'] = $row['mail'];
            $values['field-date'] = $row['data'];
            $values['radio-group-1'] = $row['radknop1'];
            $values['radio-group-2'] = $row['radknop2'];
            $values['field-name-4'] = $row['spisok'];
            $values['field-name-2'] = $row['imya2'];
            $values['check-1'] = $row['soglas'];
        }
      } catch(PDOException $e) {
          echo 'Ошибка: ' . $e->getMessage();
      }
    
    printf('Вход с логином %s', $_SESSION['login']);
  }

  	include('form.php');
}
else{

  $errors = FALSE;

  if (!preg_match("/^[-a-zA-Z]+$/",$_POST['field-name'])){
    setcookie('1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/",$_POST['field-email'])){
    setcookie('2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } 
  if (!preg_match("/^(\d{1,2})\.(\d{1,2})(?:\.(\d{4}))?$/",$_POST['field-date'])){
    setcookie('3_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['field-name'])) {
    setcookie('field-name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-name_value', $_POST['field-name'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-email'])) {
    setcookie('field-email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-email_value', $_POST['field-email'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-date'])) {
    setcookie('field-date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-date_value', $_POST['field-date'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['radio-group-1'])) {
    setcookie('radio-group-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('radio-group-1_value', $_POST['radio-group-1'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['radio-group-2'])) {
    setcookie('radio-group-2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('radio-group-2_value', $_POST['radio-group-2'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-name-4'])) {
    setcookie('field-name-4_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-name-4_value', $_POST['field-name-4'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['field-name-2'])) {
    setcookie('field-name-2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('field-name-2_value', $_POST['field-name-2'], time() + 365 * 24 * 60 * 60);
  }
  if (empty($_POST['check-1'])) {
    setcookie('check-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('check-1_value', $_POST['check-1'], time() + 365 * 24 * 60 * 60);
  }

  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('field-name_error', '', 100000);
    setcookie('1_error', '', 100000);
    setcookie('field-email_error', '', 100000);
    setcookie('2_error', '', 100000);
    setcookie('field-date_error', '', 100000);
    setcookie('3_error', '', 100000);
    setcookie('radio-group-1_error', '', 100000);
    setcookie('radio-group-2_error', '', 100000);
    setcookie('field-name-4_error', '', 100000);
    setcookie('field-name-2_error', '', 100000);
    setcookie('check-1_error', '', 100000);
  }
  
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {

  $imya = $_POST['field-name'];
  $mail = $_POST['field-email'];
  $data = $_POST['field-date'];
  $radknop1= $_POST['radio-group-1'];
  $radknop2 = $_POST['radio-group-2'];
  $spisok = $_POST['field-name-4'];
  $imya2 = $_POST['field-name-2'];
  $soglas = $_POST['check-1'];

    $user = 'u23970';
    $pass1 = '9866387';
    $db = new PDO('mysql:host=localhost;dbname=u23970', $user, $pass1, array(PDO::ATTR_PERSISTENT => true));
    $log1 = $_SESSION['login'];
    $pass24 = $_SESSION['pass'];
    try { 
      $stmt = $db->prepare("UPDATE form SET imya='$imya',mail='$mail',data='$data',radknop1='$radknop1',radknop2='$radknop2',spisok='$spisok',imya2='$imya2',soglas='$soglas' where login = '$log1' AND pass='$pass24'");
      $stmt -> execute();
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
  }

  else {
    $login = uniqid();
    $pass = rand();
    $pass2 = md5($pass);
    setcookie('login', $login);
    setcookie('pass', $pass);

    $imya = $_POST['field-name'];
    $mail = $_POST['field-email'];
    $data = $_POST['field-date'];
    $radknop1= $_POST['radio-group-1'];
    $radknop2 = $_POST['radio-group-2'];
    $spisok = $_POST['field-name-4'];
    $imya2 = $_POST['field-name-2'];
    $soglas = $_POST['check-1'];
    
    $user = 'u23970';
    $pass1 = '9866387';
    $db = new PDO('mysql:host=localhost;dbname=u23970', $user, $pass1, array(PDO::ATTR_PERSISTENT => true));
    try {
      $stmt = $db->prepare("INSERT INTO form (imya,mail,data,radknop1,radknop2,spisok,imya2,soglas, hash, login, pass) VALUE(:imya,:mail,:data,:radknop1,:radknop2,:spisok,:imya2,:soglas,:hash,:login,:pass)");
      $stmt -> execute(['imya'=>$imya,'mail'=>$mail,'data'=>$data,'radknop1'=>$radknop1,'radknop2'=>$radknop2,'spisok'=>$spisok,'imya2'=>$imya2,'soglas'=>$soglas,'hash'=>$pass2,'login'=>$login,'pass'=>$pass]);
    }
    catch (PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();

  }
}

setcookie('save', '1');

header('Location: index.php');
}
if(isset($_POST['exit'])){
session_destroy();
header('Location: login.php');
}
